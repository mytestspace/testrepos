sap.ui.define([
	"sap/ui/core/mvc/Controller"
], function(Controller) {
	"use strict";

	return Controller.extend("com.fiori.first_AI_TestApplication.controller.FirstView", {

		/**
		 * Called when a controller is instantiated and its View controls (if available) are already created.
		 * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
		 * @memberOf com.fiori.first_AI_TestApplication.view.FirstView
		 */
		//	onInit: function() {
		//
		//	},

		/**
		 * Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
		 * (NOT before the first rendering! onInit() is used for that one!).
		 * @memberOf com.fiori.first_AI_TestApplication.view.FirstView
		 */
		//	onBeforeRendering: function() {
		//
		//	},

		/**
		 * Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
		 * This hook is the same one that SAPUI5 controls get after being rendered.
		 * @memberOf com.fiori.first_AI_TestApplication.view.FirstView
		 */
		//	onAfterRendering: function() {
		//
		//	},

		/**
		 * Called when the Controller is destroyed. Use this one to free resources and finalize activities.
		 * @memberOf com.fiori.first_AI_TestApplication.view.FirstView
		 */
		//	onExit: function() {
		//
		//	}
		
		startCamera: function() {
			 if (navigator.device.capture) {
			   navigator.getUserMedia({ audio: true, video: { width: 1280, height: 720 } },
			      function(stream) {
			         var video = document.querySelector('video');
			         video.srcObject = stream;
			         video.onloadedmetadata = function(e) {
			           video.play();
			         }
			      },
			      function(err) {
			         console.log("The following error occurred: " + err.name);
			      }
			   );
			} else {
			   console.log("device not supported");
			}
		}

	});

});