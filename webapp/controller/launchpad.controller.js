sap.ui.define([
	"sap/ui/core/mvc/Controller"
], function(Controller) {
	"use strict";

	return Controller.extend("com.fiori.first_AI_TestApplication.controller.launchpad", {
		
		onInit: function(){
			//this.speaker = new RobotSpeaker();
    		//this.listener = new AudioListener();
    		
    		var SpeechRecognition = SpeechRecognition || webkitSpeechRecognition;
			var SpeechGrammarList = SpeechGrammarList || webkitSpeechGrammarList;
			var SpeechRecognitionEvent = SpeechRecognitionEvent || webkitSpeechRecognitionEvent;
			
			this.recognition = new SpeechRecognition();
			this.recognition.continuous = true;
			this.recognition.lang = 'en-US';
			this.recognition.interimResults = false;
			this.recognition.maxAlternatives = 1;
		},
		
		tile1Press: function(){
			this.getOwnerComponent().getRouter().navTo("First");
		},
		tile2Press: function(){
			this.getOwnerComponent().getRouter().navTo("second");
		},
		tile3Press: function(){
			this.getOwnerComponent().getRouter().navTo("third");
		},
		onStateChange: function(oEvent) {
			var that = this;
			if(oEvent.getParameter('state')) {
				//this.onVoice();
				this.recognition.start();
				console.log("Ready to receive a command.");
			} else {
				this.recognition.stop();
			}
			
			this.recognition.onresult = function(event) {
			  var last = event.results.length - 1;
			  var report = event.results[last][0].transcript;
			  that.callApiAI(report);
			  console.log('Confidence: ' + event.results[0][0].confidence +" Text: "+report);
			};
		},
		
		callApiAI: function(saidWords) {
			var that = this;
			var baseUrl = "https://api.dialogflow.com/v1/";
			var accessToken = "1e67e9a8d7544c98bfd095fd0389d671";
			
			$.ajax({
				type: "POST",
				url: baseUrl + "query?v=20150910",
				contentType: "application/json; charset=utf-8",
				dataType: "json",
				headers: {
					"Authorization": "Bearer " + accessToken
				},
				data: JSON.stringify({ query: saidWords, lang: "en", sessionId: "runbarry"}),
				success: function(data) {
				//	console.log(JSON.stringify(data, undefined, 2));
					that.performAction(data.result.metadata.intentName);
				},
				error: function() {
					console.log("Internal Server Error");
				}
			});
		},
		
		performAction: function(intentName){
			switch(intentName)
			{
				case "showCumulativeTotals" :
				{
					this.tile1Press();
					break;
				}
				case "showAccPayables" :
				{
					this.tile2Press();
					break;
				}
				case "showAccReceivables" :
				{
					this.tile3Press();
					break;
				}
				case "back" :
				{
					this.goToPreviousPage();
				}
			}
		},
		
		goToPreviousPage: function(){
			window.history.go(-1);
		},
		
		onVoice: function(oEvent) {
	      this.listener.listen("en", function(text) {
	        console.log("This will be an issue fix: " + text);
	      });
		}
	});
});